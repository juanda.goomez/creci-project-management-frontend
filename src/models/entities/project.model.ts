import { UserModel } from './user.model';

export class ProjectModel {
    public id: string;
    public name: string;
    public description: string;
    public status: string;
    public time: string;
    public created: Date;
    public developers:Array<UserModel> ;



    constructor(name: string, status: string) {
        this.id="";
        this.name = name;
        this.status = status;
        this.time = "";
        this.description = "";
        this.created=new Date();
        this.developers=new Array();

    }


}