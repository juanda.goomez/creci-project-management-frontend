export class UserModel {
    public id: string;
    public firstName: string;
    public lastName: string;
    public  email: string;
    public  phone: string;
    public projects: any;
    public role:string;

   
    constructor(developer) {
        this.firstName = developer.firstName;
        this.lastName = developer.lastName;
        this.email = developer.email;
        this.phone = developer.phone;
        this.role=developer.role;
      
    }


}