import { UserModel } from './user.model';
import { ProjectModel } from './project.model';

export class TaskModel {
    public id: string;
    public name: string;
    public description: string;
    public status: string;
    public time: string;
    public created: Date;
    public timeReportDate:Date;
    public developer:UserModel;
    public project:ProjectModel;



    constructor(name: string, status: string) {
        this.id="";
        this.name = name;
        this.status = status;
        this.time = "";
        this.description = "";
        this.created=new Date();
        this.developer=null;
        this.project=null;
        this.timeReportDate=null;

    }


}