import { ScrumboardLabel } from '../pages/scrumboard/interfaces/scrumboard-label.interface';
import { Scrumboard } from '../pages/scrumboard/interfaces/scrumboard.interface';




export const scrumboardLabels: ScrumboardLabel[] = [
  {
    label: 'High Priority',
    bgClass: 'bg-amber',
    textClass: 'text-amber-contrast',
  },
  {
    label: 'Blocked',
    bgClass: 'bg-red',
    textClass: 'text-red-contrast',
  },
  {
    label: 'Approved',
    bgClass: 'bg-green',
    textClass: 'text-green-contrast',
  },
  {
    label: 'Ready',
    bgClass: 'bg-cyan',
    textClass: 'text-cyan-contrast',
  },
  {
    label: 'Deployed',
    bgClass: 'bg-purple',
    textClass: 'text-purple-contrast',
  }
];



export const scrumboards: Scrumboard[] = [
  {
    id: 1,
    label: 'Creci',
    children: [
      {
        id: "1",
        label: 'Proyectos',
        childrenProjects: []

      },
      {
        id: "2",
        label: 'Tareas',
        childrenProjects: []
      }
    ]
  }
];
