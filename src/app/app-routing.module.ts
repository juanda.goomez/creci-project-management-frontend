import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CustomLayoutComponent } from './custom-layout/custom-layout.component';
import { RoleAuthGuard } from './shared/auth/roleAuth-guard.service';
import { VexRoutes } from 'src/@vex/interfaces/vex-route.interface';

const childrenRoutes: VexRoutes = [
  {
    path: 'apps',
    children: [{
      path: 'desarroladores',
      data: {
        title: 'Dashboard 1',
        expectedRole: ['Admin']
      },
      canActivate: [RoleAuthGuard],
      loadChildren: () => import('./pages/developer-crud/developer-crud.module').then(m => m.DeveloperCrudModule),
    },
    {
      path: 'scrumboard',
      loadChildren: () => import('./pages/scrumboard/scrumboard.module').then(m => m.ScrumboardModule),    
    }, 
  ]
  }
];
const routes: Routes = [
  {
    path: '',
    component: CustomLayoutComponent,
    children: childrenRoutes,
    data: {
      title: 'Dashboard 1',
      expectedRole: ['Admin', 'User']
    },
    canActivate: [RoleAuthGuard]
  },
  {
    path: 'login',
    loadChildren: () => import('./pages/auth/login/login.module').then(m => m.LoginModule),
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    // preloadingStrategy: PreloadAllModules,
    scrollPositionRestoration: 'enabled',
    relativeLinkResolution: 'corrected',
    anchorScrolling: 'enabled'
  })],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
