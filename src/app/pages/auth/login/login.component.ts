import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import icVisibility from '@iconify/icons-ic/twotone-visibility';
import icVisibilityOff from '@iconify/icons-ic/twotone-visibility-off';
import { fadeInUp400ms } from '../../../../@vex/animations/fade-in-up.animation';
import { LoginCredentialsModel } from 'src/models/auth/login-credentials-model';
import { CrudServiceService } from 'src/app/shared/api/crud-service.service';
import { TokenStorageService } from 'src/app/shared/storage-services/token-storage.service';
import { CommunicationService } from 'src/app/shared/communication/communication.service';
import { NavigationService } from 'src/@vex/services/navigation.service';
import icLayers from '@iconify/icons-ic/twotone-layers';
import icPersonOutline from '@iconify/icons-ic/twotone-person-outline';
import icAssessment from '@iconify/icons-ic/twotone-assessment';
@Component({
  selector: 'vex-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    fadeInUp400ms
  ]
})
export class LoginComponent implements OnInit {

  form: FormGroup;

  inputType = 'password';
  visible = false;

  icVisibility = icVisibility;
  icVisibilityOff = icVisibilityOff;
  private loginInfo: LoginCredentialsModel;

  constructor(private router: Router,
    private fb: FormBuilder,
    private cd: ChangeDetectorRef,
    private snackbar: MatSnackBar,
    private apiService: CrudServiceService,
    private tokenStorage: TokenStorageService,
    private communicationService: CommunicationService,
    private navigationService: NavigationService,
  ) { }

  ngOnInit() {
    this.form = this.fb.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  async send() {
    this.loginInfo = new LoginCredentialsModel(this.form.get("email").value, this.form.get('password').value);
    const path = 'users/authenticate'
    await this.apiService.auth(path, this.loginInfo).subscribe(
      async result => {
        await this.saveTokenStorage(result);
        this.configNavigation(result.role);
        if (result.role === 'User')
          this.router.navigate(['/apps/scrumboard/1']);
        else
          this.router.navigate(['/apps/desarroladores']);

      }, error => {
        if (error.status == 400)
          this.snackbar.open('Email o contraseña incorrecta intenta nuevamente.', 'Credenciales invalidas', {
            duration: 10000
          });
        else
          this.snackbar.open('Parece que tenemos problemas con la conexion al servidor.', 'Problema de conexion', {
            duration: 10000
          });
      }
    )





  }

  async saveTokenStorage(result) {
    this.tokenStorage.saveToken(result.jwtToken);
    this.tokenStorage.saveUsername(result.email);
    this.tokenStorage.saveCompleteName(result.firstName);
    this.tokenStorage.saveID(result.id);
    this.tokenStorage.saveAuthorities(result.role);
  }


  toggleVisibility() {
    if (this.visible) {
      this.inputType = 'password';
      this.visible = false;
      this.cd.markForCheck();
    } else {
      this.inputType = 'text';
      this.visible = true;
      this.cd.markForCheck();
    }
  }


  configNavigation(role) {
    if (role === 'Admin') {
      this.navigationService.items = [
        {
          type: 'subheading',
          label: 'Gestión de Proyectos',
          children: [
            {
              type: 'link',
              label: 'Desarrolladores',
              route: '/apps/desarroladores',
              icon: icPersonOutline
            },
            {
              type: 'link',
              label: 'Proyectos',
              route: '/apps/scrumboard/1',
              icon: icAssessment,
            },
          ]
        }
      ];
    }
    else {
      this.navigationService.items = [
        {
          type: 'link',
          label: 'Proyectos y tareas',
          route: '/apps/scrumboard/1',
          icon: icLayers,
          routerLinkActiveOptions: { exact: true }
        }
      ];
    }
  }

}
