import { Component, Inject, OnInit, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import icMoreVert from '@iconify/icons-ic/twotone-more-vert';
import icClose from '@iconify/icons-ic/twotone-close';
import icPrint from '@iconify/icons-ic/twotone-print';
import icDownload from '@iconify/icons-ic/twotone-cloud-download';
import icDelete from '@iconify/icons-ic/twotone-delete';
import icPhone from '@iconify/icons-ic/twotone-phone';
import icPerson from '@iconify/icons-ic/twotone-person';
import icMail from '@iconify/icons-ic/twotone-mail';
import { UserModel } from 'src/models/entities/user.model';
import icVisibility from '@iconify/icons-ic/twotone-visibility';
import icVisibilityOff from '@iconify/icons-ic/twotone-visibility-off';
import { CrudServiceService } from 'src/app/shared/api/crud-service.service';
import { InfoMessagesService } from 'src/app/shared/messages/info-messages.service';

@Component({
  selector: 'developer-create-update',
  templateUrl: './developer-create-update.component.html',
  styleUrls: ['./developer-create-update.component.scss']
})
export class DeveloperCreateUpdateComponent implements OnInit {

  static id = 100;

  inputType = 'password';
  visible = false;

  icVisibility = icVisibility;
  icVisibilityOff = icVisibilityOff;

  form: FormGroup;
  mode: 'create' | 'update' = 'create';

  icMoreVert = icMoreVert;
  icClose = icClose;

  icPrint = icPrint;
  icDownload = icDownload;
  icDelete = icDelete;
  icMail = icMail;
  icPerson = icPerson;
  icPhone = icPhone;

  constructor(@Inject(MAT_DIALOG_DATA) public defaults: any,
    private dialogRef: MatDialogRef<DeveloperCreateUpdateComponent>,
    private fb: FormBuilder, private cd: ChangeDetectorRef, private apiService: CrudServiceService, private message: InfoMessagesService) {
  }

  ngOnInit() {
    if (this.defaults) {
      this.mode = 'update';
    } else {
      this.defaults = {} as UserModel;
    }

    this.form = this.fb.group({
      firstName: [this.defaults.firstName || ''],
      lastName: [this.defaults.lastName || ''],
      password: this.defaults.password || '',
      confirmPassword: this.defaults.confirmPassword || '',
      email: this.defaults.email || '',
      phone: this.defaults.phone || ''
    });
  }

  save() {
    if (this.mode === 'create') {
      this.createDeveloper();
    } else if (this.mode === 'update') {
      this.updateDeveloper();
    }
  }

  toggleVisibility() {
    if (this.visible) {
      this.inputType = 'password';
      this.visible = false;
      this.cd.markForCheck();
    } else {
      this.inputType = 'text';
      this.visible = true;
      this.cd.markForCheck();
    }
  }

  createDeveloper() {
    const developer = this.form.value;
    developer.role = 'User';
    const path = 'users/register-developer'
    this.apiService.createModel(path, developer).subscribe(result => {
      this.dialogRef.close(true);
    },
      error => {
        this.message.getInfoMessagePersonalized("error", "Por favor revisa los datos que ingresaste, todos los campos son requeridos");
      })

  }

  updateDeveloper() {
    const developer = this.form.value;
    developer.id = this.defaults.id;
    const path = 'users/'.concat(developer.id);
    this.apiService.putModel(path, developer).subscribe(result => {
      this.dialogRef.close(true);
    },
      error => {
        this.message.getInfoMessagePersonalized("error", "Por favor revisa los datos que ingresaste, todos los campos son requeridos");
      })
  }

  isCreateMode() {
    return this.mode === 'create';
  }

  isUpdateMode() {
    return this.mode === 'update';
  }
}
