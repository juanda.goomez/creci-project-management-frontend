import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { DeveloperCrudComponent } from './developer-crud.component';


describe('DeveloperCrudComponent', () => {
  let component: DeveloperCrudComponent;
  let fixture: ComponentFixture<DeveloperCrudComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DeveloperCrudComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeveloperCrudComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
