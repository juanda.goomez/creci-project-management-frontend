import { AfterViewInit, Component, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatDialog } from '@angular/material/dialog';
import { TableColumn } from '../../../@vex/interfaces/table-column.interface';
import icEdit from '@iconify/icons-ic/twotone-edit';
import icDelete from '@iconify/icons-ic/twotone-delete';
import icSearch from '@iconify/icons-ic/twotone-search';
import icAdd from '@iconify/icons-ic/twotone-add';
import icFilterList from '@iconify/icons-ic/twotone-filter-list';
import { SelectionModel } from '@angular/cdk/collections';
import icMoreHoriz from '@iconify/icons-ic/twotone-more-horiz';
import icFolder from '@iconify/icons-ic/twotone-folder';
import { fadeInUp400ms } from '../../../@vex/animations/fade-in-up.animation';
import { MAT_FORM_FIELD_DEFAULT_OPTIONS, MatFormFieldDefaultOptions } from '@angular/material/form-field';
import { stagger40ms } from '../../../@vex/animations/stagger.animation';
import { FormControl } from '@angular/forms';
import { untilDestroyed } from 'ngx-take-until-destroy';
import icPhone from '@iconify/icons-ic/twotone-phone';
import icMail from '@iconify/icons-ic/twotone-mail';
import icMap from '@iconify/icons-ic/twotone-map';
import { DeveloperCreateUpdateComponent } from './developer-create-update/developer-create-update.component';
import { CrudServiceService } from 'src/app/shared/api/crud-service.service';
import { UserModel } from 'src/models/entities/user.model';
import { InfoMessagesService } from 'src/app/shared/messages/info-messages.service';

@Component({
  selector: 'developer-crud',
  templateUrl: './developer-crud.component.html',
  styleUrls: ['./developer-crud.component.scss'],
  animations: [
    fadeInUp400ms,
    stagger40ms
  ],
  providers: [
    {
      provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
      useValue: {
        appearance: 'standard'
      } as MatFormFieldDefaultOptions
    }
  ]
})
export class DeveloperCrudComponent implements OnInit, AfterViewInit, OnDestroy {

  layoutCtrl = new FormControl('boxed');


  developers: UserModel[];

  @Input()
  columns: TableColumn<UserModel>[] = [
    { label: 'Nombre', property: 'firstName', type: 'text', visible: true, cssClasses: ['font-medium'] },
    { label: 'Apellidos', property: 'lastName', type: 'text', visible: true },
    { label: 'Email', property: 'email', type: 'text', visible: true },
    { label: 'Telefono', property: 'phone', type: 'text', visible: true, cssClasses: ['text-secondary', 'font-medium'] },
    { label: 'Actions', property: 'actions', type: 'button', visible: true }
  ];
  pageSize = 10;
  pageSizeOptions: number[] = [5, 10, 20, 50];
  dataSource: MatTableDataSource<UserModel> | null;
  selection = new SelectionModel<UserModel>(true, []);
  searchCtrl = new FormControl();



  icPhone = icPhone;
  icMail = icMail;
  icMap = icMap;
  icEdit = icEdit;
  icSearch = icSearch;
  icDelete = icDelete;
  icAdd = icAdd;
  icFilterList = icFilterList;
  icMoreHoriz = icMoreHoriz;
  icFolder = icFolder;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(private dialog: MatDialog, private apiService: CrudServiceService, private messages: InfoMessagesService) {
  }

  get visibleColumns() {
    return this.columns.filter(column => column.visible).map(column => column.property);
  }


  getDevelopers() {
    let path = 'users'
    this.apiService.getModel(path).subscribe(result => {
      this.dataSource.data = result;
      this.developers = result;
    },
      error => {
        this.messages.getInfoMessageError();

      })
  }

  ngOnInit() {
    this.dataSource = new MatTableDataSource();
    this.getDevelopers();
    this.searchCtrl.valueChanges.pipe(
      untilDestroyed(this)
    ).subscribe(value => this.onFilterChange(value));
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  createUserModel() {
    this.dialog.open(DeveloperCreateUpdateComponent).afterClosed().subscribe((action: boolean) => {

      if (action) {
        this.messages.getInfoMessageCreate();
        this.getDevelopers();
      }
    });
  }

  updateUserModel(developer: UserModel) {
    this.dialog.open(DeveloperCreateUpdateComponent, {
      data: developer
    }).afterClosed().subscribe(updatedUserModel => {

      if (updatedUserModel) {
        this.getDevelopers();
      }
    });
  }

  deleteUserModel(developer: UserModel) {
    let path = 'users/'.concat(developer.id);
    this.apiService.deleteModel(path).subscribe(result => {
      this.getDevelopers();
    }, error => {
      this.messages.getInfoMessageError();
    })
  }


  onFilterChange(value: string) {
    if (!this.dataSource) {
      return;
    }
    value = value.trim();
    value = value.toLowerCase();
    this.dataSource.filter = value;
  }

  toggleColumnVisibility(column, event) {
    event.stopPropagation();
    event.stopImmediatePropagation();
    column.visible = !column.visible;
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  trackByProperty<T>(index: number, column: TableColumn<T>) {
    return column.property;
  }




  ngOnDestroy() {
  }
}
