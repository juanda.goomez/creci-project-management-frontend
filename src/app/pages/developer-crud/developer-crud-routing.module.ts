import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { VexRoutes } from '../../../@vex/interfaces/vex-route.interface';
import { DeveloperCrudComponent } from './developer-crud.component';


const routes: VexRoutes = [
  {
    path: '',
    component: DeveloperCrudComponent,
    data: {
      toolbarShadowEnabled: true
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DeveloperCrudRoutingModule {
}
