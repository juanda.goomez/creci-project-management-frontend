import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

import icAssignment from '@iconify/icons-ic/twotone-assignment';

import icAdd from '@iconify/icons-ic/twotone-add';
import icDescription from '@iconify/icons-ic/twotone-description';
import icClose from '@iconify/icons-ic/twotone-close';
import { ScrumboardList } from '../../interfaces/scrumboard-list.interface';
import { Scrumboard } from '../../interfaces/scrumboard.interface';
import icMoreVert from '@iconify/icons-ic/twotone-more-vert';
import icDelete from '@iconify/icons-ic/twotone-delete';
import icImage from '@iconify/icons-ic/twotone-image';
import icAttachFile from '@iconify/icons-ic/twotone-attach-file';
import icInsertComment from '@iconify/icons-ic/twotone-insert-comment';
import icStar from '@iconify/icons-ic/twotone-star';
import { scrumboardLabels } from 'src/app/static-data/scrumboard';
import { CrudServiceService } from 'src/app/shared/api/crud-service.service';
import { InfoMessagesService } from 'src/app/shared/messages/info-messages.service';
import { UserModel } from 'src/models/entities/user.model';
import { ProjectModel } from 'src/models/entities/project.model';
import { FormBuilder, FormControl } from '@angular/forms';
import { TokenStorageService } from 'src/app/shared/storage-services/token-storage.service';
import Swal from 'sweetalert2'

@Component({
  selector: 'vex-scrumboard-dialog',
  templateUrl: './scrumboard-dialog.component.html',
  styleUrls: ['./scrumboard-dialog.component.scss']
})
export class ScrumboardDialogComponent implements OnInit {

  form = this.fb.group({
    name: null,
    description: null,
    developers: [],
    developer: null,
    project: null,
    time: null,
    timeReportDate: null,
  });

  aux: string;

  commentCtrl = new FormControl();

  icAssignment = icAssignment;
  icDescription = icDescription;
  icClose = icClose;
  icAdd = icAdd;
  icMoreVert = icMoreVert;
  icDelete = icDelete;
  icImage = icImage;
  icAttachFile = icAttachFile;
  icInsertComment = icInsertComment;
  icStar = icStar;

  labels = scrumboardLabels;

  list: ScrumboardList;
  board: Scrumboard;
  developers: Array<UserModel>;
  projects: Array<ProjectModel>;
  project: any;
  role = this.tokenStorage.getAuthorities();

  constructor(private dialogRef: MatDialogRef<ScrumboardDialogComponent>,

    @Inject(MAT_DIALOG_DATA) private data: {
      project: any;
      list: ScrumboardList;
      board: Scrumboard;
    },
    private fb: FormBuilder,
    private tokenStorage: TokenStorageService,

    private apiService: CrudServiceService,
    private messages: InfoMessagesService) { }

  ngOnInit() {
    if (this.role !== 'User')
      this.getDevelopers();
    else {
      this.form.get("name").disable();
      this.form.get("description").disable();

    }
    this.list = this.data.list;
    this.board = this.data.board;
    if (this.list.label === 'Tareas' && this.role !== 'User')
      this.getProjects();
    this.project = this.data.project;

   
    if (this.project.developer !== undefined) {
      this.form.patchValue({
        name: this.project.name,
        description: this.project.description,
        time: this.project.time,
        timeReportDate: this.project.timeReportDate,
        project: this.project.project,
        developer: this.project.developer.id,
        created: this.project.created || null,
        developers: this.project.developers || []
      });
    } else {
      this.form.patchValue({
        name: this.project.name,
        description: this.project.description,
        time: this.project.time,
        timeReportDate: this.project.timeReportDate,
        project: this.project.project,
        developer: null,
        created: this.project.created || null,
        developers: this.project.developers || []
      });
    }



  }



  reportTime() {
    Swal.fire({
      title: 'Envia el reporte del tiempo utilizado en esta tarea (H)',
      input: 'text',
      inputAttributes: {
        autocapitalize: 'off',
        type: 'number'
      },
      showCancelButton: true,
      confirmButtonText: 'Enviar',
      cancelButtonText: 'Cancelar',
      showLoaderOnConfirm: true,
      preConfirm: (time) => {
        this.apiReportTime(time);
      },

    })
  }


  apiReportTime(time) {
    let path = 'tasks/'.concat(this.project.id).concat('/').concat(time);
    this.apiService.createModel(path, null).subscribe(result => {
      this.messages.getInfoMessagePersonalized("success", "Reporte exitoso")
      this.dialogRef.close(this.form.value);
    }, error => {
      this.messages.getInfoMessagePersonalized("error", "Ya han pasado pasado mas de 24 minutos para modificar este reporte");
    })
  }

  apiDeleteTime() {
    let path = 'tasks/'.concat(this.project.id);
    this.apiService.deleteModel(path).subscribe(result => {
      this.messages.getInfoMessagePersonalized("success", "El reporte se elimino")
      this.dialogRef.close(this.form.value);
    }, error => {
      this.messages.getInfoMessagePersonalized("error", "Ya han pasado pasado mas de 24 minutos para eliminar este reporte");
    })
  }

  deleteReport() {
    Swal.fire({
      title: '¿Estas seguro de eliminar este reporte?',
      text: "Esta acción no tiene reversa!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si , eliminar!',
      cancelButtonText: 'cancelar'
    }).then((result) => {
       this.apiDeleteTime();
    })
  }


  getProjects() {
    let path = 'projects'
    this.apiService.getModel(path).subscribe(result => {
      this.projects = result;
    },
      error => {
        this.messages.getInfoMessageError();
      })
  }

  getDevelopers() {
    let path = 'users'
    this.apiService.getModel(path).subscribe(result => {
      this.developers = result;
    },
      error => {
        this.messages.getInfoMessageError();

      })
  }

  save() {
    if (this.list.label === 'Tareas')
      this.updateTask();
    else
      this.updateProject();



  }





  updateTask() {
    this.project.name = this.form.get("name").value;
    this.project.description = this.form.get("description").value;
    if (this.form.get("developer").value != undefined)
      this.project.developer = this.form.get("developer").value;
    if (this.form.get("project").value != undefined)
      this.project.project = this.form.get("project").value;
    let path = "tasks/".concat(this.project.id);
    this.apiService.putModel(path, this.project).subscribe(result => {
      this.messages.getInfoMessageUpdate();
      this.dialogRef.close(this.form.value);

    }, error => {
      this.messages.getInfoMessageError();
    })


  }

  updateProject() {
    this.project.name = this.form.get("name").value;
    this.project.description = this.form.get("description").value;
    this.project.developers = new Array();
    this.form.get("developers").value.forEach(element => {
      this.project.developers.push(element.id);
    });
    let path = "projects/".concat(this.project.id);
    this.apiService.putModel(path, this.project).subscribe(result => {
      this.messages.getInfoMessageUpdate();
      this.dialogRef.close(this.form.value);

    }, error => {
      this.messages.getInfoMessageError();
    })
  }

}
