import { ScrumboardCard } from './scrumboard-card.interface';
import { ProjectModel } from 'src/models/entities/project.model';
import { TaskModel } from 'src/models/entities/task.model';

export interface ScrumboardList {
  id: string;
  label: string;
  childrenProjects: any[];
}
