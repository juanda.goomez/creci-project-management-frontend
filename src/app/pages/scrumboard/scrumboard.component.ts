import { Component, OnInit, TemplateRef } from '@angular/core';
import { ScrumboardList } from './interfaces/scrumboard-list.interface';
import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { ScrumboardCard } from './interfaces/scrumboard-card.interface';
import { trackById } from '../../../@vex/utils/track-by';
import icNotifications from '@iconify/icons-ic/twotone-notifications';
import icInsertComment from '@iconify/icons-ic/twotone-insert-comment';
import icAttachFile from '@iconify/icons-ic/twotone-attach-file';
import { MatDialog } from '@angular/material/dialog';
import { ScrumboardDialogComponent } from './components/scrumboard-dialog/scrumboard-dialog.component';
import { filter, map } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';
import { Scrumboard } from './interfaces/scrumboard.interface';
import icAdd from '@iconify/icons-ic/twotone-add';
import { PopoverService } from '../../../@vex/components/popover/popover.service';
import icClose from '@iconify/icons-ic/twotone-close';
import { FormControl } from '@angular/forms';
import icStar from '@iconify/icons-ic/twotone-star';
import icStarBorder from '@iconify/icons-ic/twotone-star-border';
import { stagger80ms } from '../../../@vex/animations/stagger.animation';
import { fadeInUp400ms } from '../../../@vex/animations/fade-in-up.animation';
import { scrumboards } from 'src/app/static-data/scrumboard';
import { UserModel } from 'src/models/entities/user.model';
import { CrudServiceService } from 'src/app/shared/api/crud-service.service';
import { InfoMessagesService } from 'src/app/shared/messages/info-messages.service';
import { ProjectModel } from 'src/models/entities/project.model';
import { TaskModel } from 'src/models/entities/task.model';
import { TokenStorageService } from 'src/app/shared/storage-services/token-storage.service';

@Component({
  selector: 'vex-scrumboard',
  templateUrl: './scrumboard.component.html',
  styleUrls: ['./scrumboard.component.scss'],
  animations: [
    stagger80ms,
    fadeInUp400ms
  ]
})
export class ScrumboardComponent implements OnInit {

  static nextId = 100;

  developers: UserModel[];
  projects: ProjectModel[];
  tasks: TaskModel[];
  project: ProjectModel;
  task: TaskModel;


  board$ = this.route.paramMap.pipe(
    map(paramMap => +paramMap.get('scrumboardId')),
    map(scrumboardId => scrumboards.find(board => board.id === scrumboardId))
  );

  addCardCtrl = new FormControl();
  addListCtrl = new FormControl();

  trackById = trackById;
  icNotifications = icNotifications;
  icInsertComment = icInsertComment;
  icAttachFile = icAttachFile;
  icAdd = icAdd;
  icClose = icClose;
  icStar = icStar;
  icStarBorder = icStarBorder;
  role = this.tokenStorage.getAuthorities();



  constructor(private dialog: MatDialog,
    private route: ActivatedRoute,
    private popover: PopoverService,
    private apiService: CrudServiceService,
    private messages: InfoMessagesService,
    private tokenStorage: TokenStorageService) { }

  ngOnInit() {
    if (this.role != 'User') {
      this.getProjects();
      this.getDevelopers();
      this.getTasks();
    }
    else {
      this.getTasksDeveloper();
      this.getProjectsDeveloper();
    }
  }

  open(board: Scrumboard, list: ScrumboardList, project: any) {
    this.addCardCtrl.setValue(null);

    this.dialog.open(ScrumboardDialogComponent, {
      data: { project, list, board },
      width: '700px',
      maxWidth: '100%',
      disableClose: true
    }).beforeClosed().pipe(
      filter<any>(Boolean)
    ).subscribe(value => {
      if (this.role !== 'User') {
        this.getProjects();

        this.getTasks();
      } else
        this.getTasksDeveloper();

    });
  }

  drop(event: CdkDragDrop<ScrumboardCard[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex);
    }
  }

  dropList(event: CdkDragDrop<ScrumboardList[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex);
    }
  }

  getConnectedList(board: Scrumboard) {
    return board.children.map(x => `${x.id}`);
  }

  openAddCard(list: ScrumboardList, content: TemplateRef<any>, origin: HTMLElement) {
    this.popover.open({
      content,
      origin,
      position: [
        {
          originX: 'center',
          originY: 'bottom',
          overlayX: 'center',
          overlayY: 'bottom'
        },
        {
          originX: 'center',
          originY: 'bottom',
          overlayX: 'center',
          overlayY: 'top',
        },
      ]
    });
  }

  openAddList(board: Scrumboard, content: TemplateRef<any>, origin: HTMLElement) {
    this.popover.open({
      content,
      origin,
      position: [
        {
          originX: 'center',
          originY: 'bottom',
          overlayX: 'center',
          overlayY: 'top'
        },
        {
          originX: 'center',
          originY: 'bottom',
          overlayX: 'center',
          overlayY: 'top',
        },
      ]
    });
  }

  createProject(list: ScrumboardList, close: () => void) {
    this.project = new ProjectModel(this.addCardCtrl.value, "CREATED");
    let path = 'projects/create-project'
    this.apiService.createModel(path, this.project).subscribe(result => {
      this.messages.getInfoMessageCreate();
      this.addCardCtrl.setValue(null);
      this.getProjects();
    }, error => {
      this.messages.getInfoMessageError();
    })

    if (!this.addCardCtrl.value) {
      return;
    }
    close();

  }

  createTask(list: ScrumboardList, close: () => void) {
    this.task = new TaskModel(this.addCardCtrl.value, "CREATED");
    let path = 'tasks/create-task'
    this.apiService.createModel(path, this.task).subscribe(result => {
      this.messages.getInfoMessageCreate();
      this.addCardCtrl.setValue(null);
      if (this.role !== 'User')
        this.getTasks();
      else
        this.getTasksDeveloper();
    }, error => {
      this.messages.getInfoMessageError();
    })

    if (!this.addCardCtrl.value) {
      return;
    }
    close();

  }


  toggleStar(board: Scrumboard) {
    board.starred = !board.starred;
  }


  getDevelopers() {
    let path = 'users'
    this.apiService.getModel(path).subscribe(result => {
      this.developers = result;
    },
      error => {
        this.messages.getInfoMessageError();

      })
  }

  getProjects() {
    let path = 'projects'
    this.apiService.getModel(path).subscribe(result => {
      this.projects = result;
      scrumboards[0].children[0].childrenProjects = this.projects;
    },
      error => {
        this.messages.getInfoMessageError();

      })
  }


  getTasks() {
    let path = 'tasks'
    this.apiService.getModel(path).subscribe(result => {
      this.tasks = result;
      scrumboards[0].children[1].childrenProjects = this.tasks;
    },
      error => {
        this.messages.getInfoMessageError();

      })
  }

  getTasksDeveloper() {
    let path = 'tasks/'.concat(this.tokenStorage.getId());
    this.apiService.createModel(path, null).subscribe(result => {
      this.tasks = result;
      scrumboards[0].children[1].childrenProjects = this.tasks;
    },
      error => {
        this.messages.getInfoMessageError();

      })
  }

  getProjectsDeveloper() {
    let path = 'projects/'.concat(this.tokenStorage.getId());
    this.apiService.getModel(path).subscribe(result => {
      this.projects = result;
      scrumboards[0].children[0].childrenProjects = this.projects;
    },
      error => {
        this.messages.getInfoMessageError();

      })
  }
}
