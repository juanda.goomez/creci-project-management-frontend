import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment.prod';
import { TokenStorageService } from '../storage-services/token-storage.service';

@Injectable({
  providedIn: 'root'
})
export class CrudServiceService {
  private serverURL = environment.serverUrl;
  requestOptions = {
    headers: { 'Content-Type': 'application/json' },
    credentials: 'include'

  };
  requestOptionsAuthenticateGet = {
    method: 'GET',
    headers: { 'Content-Type': 'application/json','Authorization':`Bearer ${this.token.getToken()}`},
    credentials: 'include'

  };
   requestOptionsAuthenticatePost = {
    headers: { 'Content-Type': 'application/json','Authorization':`Bearer ${this.token.getToken()}`},
    credentials: 'include'

  };



  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': this.token.getToken()
    })
  };

  constructor(private http: HttpClient, private token: TokenStorageService) { }

  public buildRequestGet() {
    this.requestOptionsAuthenticateGet = {
      method: 'GET',
      headers: { 'Content-Type': 'application/json','Authorization':`Bearer ${this.token.getToken()}`},
      credentials: 'include'
  
    };
  }

  public buildRequestGPost() {
    this.requestOptionsAuthenticatePost = {
      headers: { 'Content-Type': 'application/json','Authorization':`Bearer ${this.token.getToken()}`},
      credentials: 'include'
  
    };  
  }

 


  public createModel(path, model): Observable<any> {
    this.buildRequestGPost();
    return this.http.post<any>(`${this.serverURL}${path}`,model,this.requestOptionsAuthenticatePost);
  }

  public auth(path, model): Observable<any> {
   return this.http.post<any>(`${this.serverURL}${path}`, model, this.requestOptions);
  }


  public putModel(path, model): Observable<any> {
    this.buildRequestGPost();
    return this.http.put<any>(`${this.serverURL}${path}`, model, this.requestOptionsAuthenticatePost);
  }

  public getModel(path): Observable<any> {
    this.buildRequestGet();
    return this.http.get<any>(`${this.serverURL}${path}`, this.requestOptionsAuthenticateGet);
  }

  public deleteModel(path): Observable<any> {
    this.buildRequestGPost();
    return this.http.delete<any>(`${this.serverURL}${path}`, this.requestOptionsAuthenticatePost);
  }




}
