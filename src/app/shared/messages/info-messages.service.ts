import { Injectable } from '@angular/core';
import Swal from 'sweetalert2'

@Injectable({
  providedIn: 'root'
})
export class InfoMessagesService {
  public message: string;
  public type: string;

  constructor() { }

  getInfoMessageError() {
    Swal.fire({
      title: 'Error!',
      text: 'Ocurrio un error en el sistema',
      icon: 'error',
      confirmButtonText: 'Aceptar'
    })
  }

  getInfoMessageCreate() {
     Swal.fire({
      position: 'center',
      icon: 'success',
      title: 'El registro ha sido guardado',
      showConfirmButton: false,
      timer: 3000
    })
  }

  getInfoMessageDelete(){
    Swal.fire({
      position: 'center',
      icon: 'success',
      title: 'El registro ha sido elimindado',
      showConfirmButton: false,
      timer: 3000
    })
  }

  getInfoMessageUpdate(){
    Swal.fire({
      position: 'center',
      icon: 'success',
      title: 'El registro ha sido actualizado',
      showConfirmButton: false,
      timer: 3000
    })
  }



  getInfoMessagePersonalized(typeMessage, message) {
    Swal.fire({
      position: 'center',
      icon: typeMessage,
      title: message,
      showConfirmButton: false,
      timer: 3000
    })
  }
}
