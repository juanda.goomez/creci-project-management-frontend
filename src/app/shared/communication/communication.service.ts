import { Injectable } from '@angular/core';
import { UserModel } from 'src/models/entities/user.model';

@Injectable({
  providedIn: 'root'
})
export class CommunicationService {
  
  public user:UserModel;
   
  constructor() { }


   setUser(user:UserModel){
     this.user=user;
   }

   getUser(){
     return this.user;
   }


}
