import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { VexModule } from '../@vex/vex.module';
import { HttpClientModule } from '@angular/common/http';
import { CustomLayoutModule } from './custom-layout/custom-layout.module';
import { LoginModule } from './pages/auth/login/login.module';
import { RoleAuthGuard } from './shared/auth/roleAuth-guard.service';
import { DeveloperCrudModule } from './pages/developer-crud/developer-crud.module';
import { ScrumboardModule } from './pages/scrumboard/scrumboard.module';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    LoginModule,
    DeveloperCrudModule,
    ScrumboardModule,

    // Vex
    VexModule,
    CustomLayoutModule
  ],
  providers: [
    RoleAuthGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
