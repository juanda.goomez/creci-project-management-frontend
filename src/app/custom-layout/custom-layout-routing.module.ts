import { NgModule } from '@angular/core';
import { Routes,RouterModule } from '@angular/router';
import { CustomLayoutComponent } from './custom-layout.component';
import { RoleAuthGuard } from '../shared/auth/roleAuth-guard.service';

const routes: Routes = [
  {
    path: '',
    component:CustomLayoutComponent,
    data: {
      title: 'Dashboard 1',
      expectedRole: ['Admin', 'User']
    },
    canActivate: [RoleAuthGuard]
  
   
  } ];


  @NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
  })
export class CustomLayoutRoutingModule { }
